# fake-lsb-core - Pseudo package for LSB Core

Some programs like the flexlm license manager need the `lsb-core` package
and a library like `ld-lsb-x86-64.so` which is no longer provided in Debian.
This package provides that and does virtually nothing else.
It only concerns the amd64 architecture.

* For Debian 12 (Bookworm)

     ```bash
     /lib64/ld-lsb-x86-64.so.3 -> /lib/x86_64-linux-gnu/ld-linux-x86-64.so.2
     ```

* For Debian 11 (Bullseye)

     ```bash
     /lib64/ld-lsb-x86-64.so.3 -> /lib/x86_64-linux-gnu/ld-2.31.so
     ```
