
.PHONY: all clean

all:
	./make-package-debian

clean:
	rm -f *.tar.gz *.deb debian-binary
